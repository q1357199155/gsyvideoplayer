package com.shuyu.gsyvideoplayer.datasource;

import android.content.Context;

import java.io.IOException;
import java.util.Map;

import tv.danmaku.ijk.media.player.IjkMediaPlayer;

/**
 * Created by zhoulikai on 17/4/27.
 * 视频源管理
 */

public interface IDataSource {
    public void setDataSource(Context context, IjkMediaPlayer mediaPlayer, String path, Map<String, String> headers) throws IOException;
}
