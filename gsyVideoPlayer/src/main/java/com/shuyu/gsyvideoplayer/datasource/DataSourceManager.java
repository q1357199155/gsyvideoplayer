package com.shuyu.gsyvideoplayer.datasource;

import android.content.Context;
import android.text.TextUtils;

import com.shuyu.gsyvideoplayer.model.GSYModel;

import java.io.IOException;
import java.util.Map;

import tv.danmaku.ijk.media.player.IjkMediaPlayer;

/**
 * Created by zhoulikai on 17/4/27.
 * 视频源管理
 */

public class DataSourceManager implements IDataSource {
    @Override
    public void setDataSource(Context context, IjkMediaPlayer mediaPlayer, String path, Map<String, String> headers) throws IOException {
        if (TextUtils.isEmpty(path)) {
            return;
        }
        if (path.startsWith(MediaDataSource.PREFIX_FLAG)) {
            mediaPlayer.setDataSource(new MediaDataSource(context, path));
        } else {
            mediaPlayer.setDataSource(path, headers);

        }

    }
}
