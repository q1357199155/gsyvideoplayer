package com.danikula.videocache.file.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.security.MessageDigest;

/**
 * Created by zhoulikai on 17/4/11.
 * 工具类,由zhoulikai add
 */

public class Tools {
    private final static String IMEI_PREFIX = "YONGYOU_PREFIX";
    private final static String IMEI_SUFFIX = "YONGYOU_IME_SUFFIX";

    /**
     * 获取设备ID,需要 权限<uses-permission android:name="android.permission.READ_PHONE_STATE" />
     *
     * @param context
     * @return
     */
    public static String getImei(Context context) {
        String imei = "";
        if (context != null) {
            imei = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE))
                    .getDeviceId();
        }

        return IMEI_PREFIX + imei + IMEI_SUFFIX;
    }

    /**
     * 加密
     *
     * @param plaintext 明文
     * @return ciphertext 密文
     */
    public final static String md5(String plaintext) {
        if (TextUtils.isEmpty(plaintext)) {
            return "";
        }
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            byte[] btInput = plaintext.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            return null;
        }
    }


}
