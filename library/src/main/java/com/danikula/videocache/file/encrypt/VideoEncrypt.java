package com.danikula.videocache.file.encrypt;

/**
 * Created by zhoulikai on 17/4/11.
 * 视频加密实现
 */

public class VideoEncrypt implements IVideoEncrypt {
    @Override
    public void liveEnCryptVideoFrame(byte[] buffer) {
        if (liveVideoHandle(buffer)) {
//            for (int i = 0; i < buffer.length; i++) {
//                byte data = buffer[i];
//
//                buffer[i] = 0;
//
//                for (int j = 7; j > 0; j--) {
//
//                    byte shift = (byte) (((data & 0xff) >>> j) & ((0x1 << (7 - j))) & 0xff);
//
//                    buffer[i] = (byte) ((buffer[i] & 0xff) | (shift & 0xff));
//                }
//            }
        }

    }

    @Override
    public void liveDecryptVideoFrame(byte[] buffer) {
        if (liveVideoHandle(buffer)) {
//            for (int i = 0; i < buffer.length; i++) {
//                byte data = buffer[i];
//
//                buffer[i] = 0;
//
//                for (byte j = 7; j > 0; j--) {
//
//                    byte shift = (byte) ((data & 0xff) << j);
//
//                    shift = (byte) ((shift & 0xff) & (((0x1 << 7) >>> (7 - j)) & 0xff));
//                    buffer[i] = (byte) ((buffer[i] & 0xff) | (shift & 0xff));
//                }
//            }
        }
    }

    @Override
    public void encryptVideoHeader(byte[] bytes) {

    }

    @Override
    public void validateVideHeader(byte[] bytes) {

    }

    @Override
    public void completeEnCryptVideoFrame(byte buffer) {

    }

    @Override
    public void completeDecryptVideoFrame(byte buffer) {

    }

    /**
     * 数据源数据取反
     *
     * @param buffer
     * @return
     */
    private boolean liveVideoHandle(byte[] buffer) {
        if (buffer == null) {
            return false;
        }
        byte temp;
        for (int i = 0; i < buffer.length; i++) {
            temp = buffer[i];
            buffer[i] = (byte) (~temp);
        }
        return true;
    }
}
