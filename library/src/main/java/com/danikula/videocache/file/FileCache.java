package com.danikula.videocache.file;

import android.text.TextUtils;

import com.danikula.videocache.Cache;
import com.danikula.videocache.Config;
import com.danikula.videocache.ProxyCacheException;
import com.danikula.videocache.file.encrypt.IVideoEncrypt;
import com.danikula.videocache.file.encrypt.VideoEncrypt;
import com.danikula.videocache.file.util.Tools;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * {@link Cache} that uses file for storing data.
 *
 * @author Alexey Danilov (danikula@gmail.com).
 */
public class FileCache implements Cache {

    private static final String TEMP_POSTFIX = ".download";

    private final DiskUsage diskUsage;
    public File file;
    private RandomAccessFile dataFile;
    //zhoulikai add start
    private IVideoEncrypt iVideoEncrypt;
    private String md5Str = "";
    //zhoulikai add end

    public FileCache(File file) throws ProxyCacheException {
        this(file, new UnlimitedDiskUsage(), null);
    }

    //zhoulikai add start
    public FileCache(File file, Config config) throws ProxyCacheException {
        this(file, new UnlimitedDiskUsage(), config);
    }
    //zhoulikai add end

    public FileCache(File file, DiskUsage diskUsage, Config config) throws ProxyCacheException {
        try {
            if (diskUsage == null) {
                throw new NullPointerException();
            }
            //zhoulikai add start
            iVideoEncrypt = new VideoEncrypt();
            //zhoulikai add end
            this.diskUsage = diskUsage;
            File directory = file.getParentFile();
            Files.makeDir(directory);
            boolean completed = file.exists();
            this.file = completed ? file : new File(file.getParentFile(), file.getName() + TEMP_POSTFIX);
            //zhoulikai add start
            if (config != null) {
                md5Str = Tools.md5(config.imei);
            }
            //zhoulikai add end
            this.dataFile = new RandomAccessFile(this.file, completed ? "r" : "rw");

            //zhoulikai add start
            //首次将MD5头写入视频文件
            if (!TextUtils.isEmpty(md5Str) && dataFile.length() == 0) {
                dataFile.seek(0);
                byte[] md5Byte = md5Str.getBytes();
                dataFile.write(md5Byte, 0, md5Byte.length);
            }
            //zhoulikai add end
        } catch (IOException e) {
            throw new ProxyCacheException("Error using file " + file + " as disc cache", e);
        }
    }

    @Override
    public synchronized int available() throws ProxyCacheException {
        try {
            return (int) dataFile.length() - md5Str.getBytes().length;
        } catch (IOException e) {
            throw new ProxyCacheException("Error reading length of file " + file, e);
        }
    }

    @Override
    public synchronized int read(byte[] buffer, long offset, int length) throws ProxyCacheException {
        try {
            dataFile.seek(md5Str.getBytes().length + offset);
            int count = dataFile.read(buffer, 0, length);
            //zhoulikai add start
            iVideoEncrypt.liveDecryptVideoFrame(buffer);
            //zhoulikai add end
            return count;
        } catch (IOException e) {
            String format = "Error reading %d bytes with offset %d from file[%d bytes] to buffer[%d bytes]";
            throw new ProxyCacheException(String.format(format, length, offset, available(), buffer.length), e);
        }
    }

    @Override
    public synchronized void append(byte[] data, int length) throws ProxyCacheException {
        try {
            if (isCompleted()) {
                throw new ProxyCacheException("Error append cache: cache file " + file + " is completed!");
            }
            dataFile.seek(available() + md5Str.getBytes().length);
            //zhoulikai add start
            iVideoEncrypt.liveEnCryptVideoFrame(data);
            //zhoulikai add end
            dataFile.write(data, 0, length);
        } catch (IOException e) {
            String format = "Error writing %d bytes to %s from buffer with size %d";
            throw new ProxyCacheException(String.format(format, length, dataFile, data.length), e);
        }
    }

    @Override
    public synchronized void close() throws ProxyCacheException {
        try {
            dataFile.close();
            diskUsage.touch(file);
        } catch (IOException e) {
            throw new ProxyCacheException("Error closing file " + file, e);
        }
    }

    @Override
    public synchronized void complete() throws ProxyCacheException {
        if (isCompleted()) {
            return;
        }

        close();
        String fileName = file.getName().substring(0, file.getName().length() - TEMP_POSTFIX.length());
        File completedFile = new File(file.getParentFile(), fileName);
        boolean renamed = file.renameTo(completedFile);
        if (!renamed) {
            throw new ProxyCacheException("Error renaming file " + file + " to " + completedFile + " for completion!");
        }
        //
        file = completedFile;
        try {
            dataFile = new RandomAccessFile(file, "r");
        } catch (IOException e) {
            throw new ProxyCacheException("Error opening " + file + " as disc cache", e);
        }
    }

    @Override
    public synchronized boolean isCompleted() {
        return !isTempFile(file);
    }

    /**
     * Returns file to be used fo caching. It may as original file passed in constructor as some temp file for not completed cache.
     *
     * @return file for caching.
     */
    public File getFile() {
        return file;
    }

    private boolean isTempFile(File file) {
        return file.getName().endsWith(TEMP_POSTFIX);
    }

}
