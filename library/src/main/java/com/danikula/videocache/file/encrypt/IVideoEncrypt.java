package com.danikula.videocache.file.encrypt;

/**
 * Created by zhoulikai on 17/4/10.
 * 视频加密接口
 */

public interface IVideoEncrypt {
    /**
     * 对视频帧进行实时加密,为了防止中间缓存文件被盗取播放，目前采用取反并反序列操作
     *
     * @param buffer
     */
    public void liveEnCryptVideoFrame(byte[] buffer);

    /**
     * 实时解密
     *
     * @param buffer
     */
    public void liveDecryptVideoFrame(byte[] buffer);

    /**
     * 对视频头进行加密，目前采用MD5(Android+IMIE+Android)方式插入视频头，使其混淆，
     * 不能分辨出视频信息
     *
     * @param bytes
     */
    public void encryptVideoHeader(byte[] bytes);

    /**
     * 验证视频头
     *
     * @param bytes
     */
    public void validateVideHeader(byte[] bytes);

    /**
     * 为了安全起见，可对下载完成后整个数据，再次加密
     *
     * @param buffer
     */
    public void completeEnCryptVideoFrame(byte buffer);


    public void completeDecryptVideoFrame(byte buffer);

}
